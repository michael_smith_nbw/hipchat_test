#!/usr/bin/env python

from distutils.core import setup
import os

# Return the first line of git describe --tags
def get_git_version():
    stream = os.popen('git describe --tags')
    for line in stream:
        lines = line.strip()
        return lines
    return '0.0'

setup(name='hipchat_post',
    version=get_git_version(),
    description='Hipchat Post',
    author='Michael Smith',
    author_email='michael.smith@nothingbutweb.com.au',
    url='http://www.python.org/sigs/distutils-sig/',
    packages=[
        'hipchat_post',
    ],
    package_data={
    },
    scripts=[
        'bin/hiphat_post',
    ]
)
