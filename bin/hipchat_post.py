import argparse
import codecs
from random import random
from os.path import expanduser
from hypchat import HypChat

class HipchatTest:

    # Constructor
    def __init__(self):
        parser = argparse.ArgumentParser(prog='missing_reports.py', description='List missing reports')
        parser.add_argument('--message', help='Message to send', nargs=1, default=None)
        parser.add_argument('--random-message', help='Select a random message from a file', nargs='+', default=None)
        parser.add_argument('--room', help='Room to connect to', nargs='+', default=['Web Dev'])
        parser.add_argument('--token', help='OAuth token file', nargs='+', default=None)
        parser.add_argument('--test', help='Test mode', action='store_true')
        self.args = parser.parse_args()
        print('__init__: self.args %s' % self.args)

    def get_token(self):
        if self.args.token:
            token_file = open(self.args.token[0])
        else:
            token_file = open(expanduser("~") + '/HIPCHAT_TOKEN')
        token = token_file.read().strip(' \n\t\r')
        print('[%s]' % token)
        return token
    
    def run(self):
        hc = HypChat(self.get_token())
        room = hc.get_room(self.args.room[0])
        if self.args.random_message:
            message_file = codecs.open(self.args.random_message[0], "r",encoding='utf-8', errors='ignore')
            messages = [msg.strip() for msg in message_file]
            print(messages)
            indx = int(random() * len(messages))
            message = messages[indx]
        if self.args.message:
            message = self.args.message[0]
        print('message: [%s]' % message)
        if not self.args.test:
            room.message('[BOT] ' + message)

if __name__ == "__main__":
    hipchat_test = HipchatTest()
    hipchat_test.run()
